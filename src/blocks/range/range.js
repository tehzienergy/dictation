$('.range__input').rangeslider({
  polyfill: false,
  onSlide: function(position, value) {
    $('.range__value span').text(value);
  },
});
