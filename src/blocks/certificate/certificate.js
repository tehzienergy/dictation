$('.test__certificate').click(function(e) {
  e.preventDefault();
  $('.certificate').fadeIn(300);
  $('body').addClass('body--fixed');
  setTimeout(function() {
    $('.certificate').css('display', 'flex');
  }, 300)
});

$('.certificate__close').click(function(e) {
  e.preventDefault();
  $('.certificate').fadeOut(300);
  $('body').removeClass('body--fixed');
});
