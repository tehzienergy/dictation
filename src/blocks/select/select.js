$('.select').select2({
  minimumResultsForSearch: -1,
  width: '100%',
  dropdownAutoWidth: true,
});

$('.select--dark').select2({
  minimumResultsForSearch: -1,
  width: '100%',
  dropdownAutoWidth: true,
  containerCssClass: 'dark',
  dropdownCssClass: "dark"
});
