$('.menu__btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('menu__btn--active');
  $('.menu__content').fadeToggle();
});

if ($(window).width() < 1200) {
  $(document).on('click', function (e) {
    var modal = $('.menu');
    if (!modal.is(e.target) &&
      modal.has(e.target).length === 0) {
      $('.menu__btn').removeClass('menu__btn--active');
      $('.menu__content').fadeOut();
    }
  });
}
