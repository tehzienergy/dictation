$('.library__filters-mobile-btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('library__filters-mobile-btn--active');
  $(this).closest('.library__filters').find('.library__filters-mobile').slideToggle('fast');
});
