$('.b-map__region').click(function(e) {
  e.preventDefault();
  $('.b-map__region').removeClass('b-map__region--active');
  $(this).addClass('b-map__region--active');
  var regionID = $(this).attr('id');
  console.log(regionID);
  $('.regions__item').removeClass('regions__item--active');
  $('[data-region="' + regionID + '"]').addClass('regions__item--active');
});
