$('.tabs__btn').click(function(e) {
  e.preventDefault();
  $('.tabs__btn').removeClass('tabs__btn--active');
  $(this).addClass('tabs__btn--active');
  $('.tabs__item').removeClass('tabs__item--active');
  $('.tabs__item').eq($(this).index()).addClass('tabs__item--active');
});
