$('.question__btn--next').click(function (e) {
  if ($('.custom-control-input').is(':checked')) {
    $('.question__motivation').removeClass('question__motivation--active');
  } else {
    e.preventDefault();
    $('.question__motivation').addClass('question__motivation--active');
  }
});

$('.custom-control-input').change(function() {
  if ($(this).is(':checked')) {
    $('.question__btn').addClass('question__btn--active');
  }
});
