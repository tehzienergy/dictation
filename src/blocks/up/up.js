$(window).scroll(function () {
  if ($(document).scrollTop() > 1000) {
    $('.up').addClass('up--active');
  } else {
    $('.up').removeClass('up--active');
  }
});

$('.up').click(function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop: 0},'slow');
});
