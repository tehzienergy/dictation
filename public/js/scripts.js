
$(".modal").on('hidden.bs.modal', function (e) {
    $(this).find("iframe").attr("src", $(this).find("iframe").attr("src"));
});

if ($(".calendar")[0]){
  $(function() {
    $('.calendar').datepicker({
      yearRange: "-100:+3",
      defaultDate: null,
      changeMonth: true,
      changeYear: true,
      dateFormat: "dd.mm.yy",
      numberOfMonths: 1,
      firstDay: 1
    });
  });
}

$('.test__certificate').click(function(e) {
  e.preventDefault();
  $('.certificate').fadeIn(300);
  $('body').addClass('body--fixed');
  setTimeout(function() {
    $('.certificate').css('display', 'flex');
  }, 300)
});

$('.certificate__close').click(function(e) {
  e.preventDefault();
  $('.certificate').fadeOut(300);
  $('body').removeClass('body--fixed');
});






(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();





$('.library__filters-mobile-btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('library__filters-mobile-btn--active');
  $(this).closest('.library__filters').find('.library__filters-mobile').slideToggle('fast');
});


$('.menu__btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('menu__btn--active');
  $('.menu__content').fadeToggle();
});

if ($(window).width() < 1200) {
  $(document).on('click', function (e) {
    var modal = $('.menu');
    if (!modal.is(e.target) &&
      modal.has(e.target).length === 0) {
      $('.menu__btn').removeClass('menu__btn--active');
      $('.menu__content').fadeOut();
    }
  });
}


$('#js-open-geotest').click(function (e) {
    e.preventDefault();
    $(this).closest('.row').hide();
    $('#geotest').attr('style', 'display : flex');
    var headerHeight = $('.header').outerHeight();
});

$('.test__close').click(function(e) {
  e.preventDefault();
  $('.test').fadeOut();
  $('#js-open-geotest').closest('.row').fadeIn('fast');
})


$('.question__btn--next').click(function (e) {
  if ($('.custom-control-input').is(':checked')) {
    $('.question__motivation').removeClass('question__motivation--active');
  } else {
    e.preventDefault();
    $('.question__motivation').addClass('question__motivation--active');
  }
});

$('.custom-control-input').change(function() {
  if ($(this).is(':checked')) {
    $('.question__btn').addClass('question__btn--active');
  }
});

$('.range__input').rangeslider({
  polyfill: false,
  onSlide: function(position, value) {
    $('.range__value span').text(value);
  },
});

$('.b-map__region').click(function(e) {
  e.preventDefault();
  $('.b-map__region').removeClass('b-map__region--active');
  $(this).addClass('b-map__region--active');
  var regionID = $(this).attr('id');
  console.log(regionID);
  $('.regions__item').removeClass('regions__item--active');
  $('[data-region="' + regionID + '"]').addClass('regions__item--active');
});

$('.select').select2({
  minimumResultsForSearch: -1,
  width: '100%',
  dropdownAutoWidth: true,
});

$('.select--dark').select2({
  minimumResultsForSearch: -1,
  width: '100%',
  dropdownAutoWidth: true,
  containerCssClass: 'dark',
  dropdownCssClass: "dark"
});


$('.subscribe__input').change(function() {
  $(this).addClass('subscribe__input--active');
  if( !$(this).val() ) {
    $(this).removeClass('subscribe__input--active');
  }
});

$('.tabs__btn').click(function(e) {
  e.preventDefault();
  $('.tabs__btn').removeClass('tabs__btn--active');
  $(this).addClass('tabs__btn--active');
  $('.tabs__item').removeClass('tabs__item--active');
  $('.tabs__item').eq($(this).index()).addClass('tabs__item--active');
});




$('.user__notifications-btn').click(function() {
  $('.user').toggleClass('user--active');
});

$(window).scroll(function () {
  if ($(document).scrollTop() > 1000) {
    $('.up').addClass('up--active');
  } else {
    $('.up').removeClass('up--active');
  }
});

$('.up').click(function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop: 0},'slow');
});
